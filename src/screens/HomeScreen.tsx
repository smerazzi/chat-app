import { FlatList, StatusBar, SafeAreaView } from 'react-native';
import React, { useEffect, useState } from 'react';
import UserChat from '../components/UserChat';
import HorizontalSeparator from '../components/HorizontalSeparator';
import { HomeScreenStackProps } from '../types';

type User = {
  imageUrl: string;
  name: string;
  lastMessage: string;
  id: string;
};

export default function HomeScreen({}: HomeScreenStackProps) {
  const [people, setPeople] = useState([]);

  useEffect(() => {
    const getRandomPeople = () => {
      fetch('https://randomuser.me/api/?results=20')
        .then(response => {
          if (!response.ok) {
            throw new Error(
              `Error in requesting API. Status ${response.status}`,
            );
          }
          return response.json();
        })
        .then(data => {
          const mappedData = data.results.map(
            (item: {
              picture: { large: any };
              name: { first: any; last: any };
              id: { value: any };
            }) =>
              ({
                imageUrl: item.picture.large,
                name: `${item.name.first} ${item.name.last}`,
                lastMessage: 'Last Message',
                id: item.id.value,
              } as User),
          );
          setPeople(mappedData);
        });
    };
    getRandomPeople();
  }, []);

  return (
    <SafeAreaView className="flex bg-slate-100">
      <StatusBar barStyle="light-content" backgroundColor="white" />
      <HorizontalSeparator />
      <FlatList<User>
        data={people}
        renderItem={({ item }) => <UserChat {...item} />}
        ItemSeparatorComponent={HorizontalSeparator}
      />
    </SafeAreaView>
  );
}
