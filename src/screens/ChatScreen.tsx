import { FlatList, StatusBar, SafeAreaView, Platform, ActivityIndicator } from 'react-native';
import React, { useCallback, useEffect, useState } from 'react';
import { ChatScreenStackProps } from '../types';
import Conversation from '../components/Conversation';

type Message = {
  direction: string;
  text: string;
  id: string;
};

export default function ChatScreen({}: ChatScreenStackProps) {
  const [messages, setMessages] = useState<Message[]>([]);
  const [page, setPage] = useState<number>(1);
  const [loading, setLoading] = useState(true);

  const renderItem = useCallback(({ item }) => <Conversation {...item} />, []);

  const getRandomMessages = useCallback(() => {
    fetch(`https://quotable.io/quotes?page=${page}`)
      .then(response => {
        if (!response.ok) {
          setLoading(false);
          throw new Error(`Error in requesting API. Status ${response.status}`);
        }
        return response.json();
      })
      .then(data => {
        const mappedData = data.results.map(
          (item: { content: any; length: number; __id: any }) =>
            ({
              text: item.content,
              direction: item.length % 2 === 0 ? 'INCOMING' : 'OUTGOING',
              id: item.__id,
            } as Message),
        );
        setLoading(false);
        setMessages(prevMessages => prevMessages.concat(mappedData));
      });
  }, [page]);

  useEffect(() => {
    getRandomMessages();
  }, [getRandomMessages]);

  return (
    <SafeAreaView className={`flex ${Platform.OS === 'android' && 'pb-5'}`}>
      <StatusBar barStyle="light-content" backgroundColor="white" />
      {loading && (
        <ActivityIndicator size="large" color="#94a3b8" animating={loading} />
      )}
      <FlatList<Message>
        data={messages}
        renderItem={renderItem}
        inverted
        onEndReached={() => {
          setLoading(true);
          setPage(prevValue => prevValue + 1);
          getRandomMessages();
        }}
        initialNumToRender={10}
        maxToRenderPerBatch={15}
        windowSize={20}
      />
    </SafeAreaView>
  );
}
