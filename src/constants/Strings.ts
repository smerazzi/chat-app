import i18n from '../services/I18nService';

export const STRINGS = Object.freeze({
  SCREENS: {
    HEADER_TITLES: {
      HOME: i18n.t('screens.header_titles.home'),
      CHAT: i18n.t('screens.header_titles.home'),
    },
    NAMES: {
      HOME: 'Home',
      CHAT: 'Chat',
    },
  },
});
