import { View, Text } from 'react-native';
import React from 'react';

type UserProps = {
  direction: string;
  text: string;
};

const Conversation = (props: UserProps) => {
  return (
    <View
      className={`flex-row px-5 pt-8 ${
        props.direction === 'INCOMING' ? 'self-start' : 'self-end'
      }`}>
      <View
        className={`rounded-lg p-3 ${
          props.direction === 'INCOMING' ? 'bg-slate-600' : 'bg-slate-400'
        }`}>
        <Text className="text-lg font-bold text-center text-slate-100">
          {props.text}
        </Text>
      </View>
    </View>
  );
};

export default Conversation;
