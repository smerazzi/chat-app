import { View } from 'react-native';
import React from 'react';

const HorizontalSeparator = () => {
  return <View className="h-0.5 w-full bg-slate-700" />;
};

export default HorizontalSeparator;
