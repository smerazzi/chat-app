import { View, Text, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { HomeScreenStackProps } from '../types';

type UserProps = {
  imageUrl: string;
  name: string;
  lastMessage: string;
};

const UserChat = (props: UserProps) => {
  const navigation = useNavigation<HomeScreenStackProps['navigation']>();
  return (
    <TouchableOpacity
      className="flex-row px-5 pt-3 pb-1"
      onPress={() =>
        navigation.navigate('Chat', {
          name: props.name,
        })
      }>
      <Image
        source={{ uri: props.imageUrl }}
        resizeMode="contain"
        className="w-14 h-14 rounded-full"
      />
      <View className="ml-10 items-start">
        <Text className="text-lg font-bold text-center text-black">
          {props.name}
        </Text>
        <Text className="text-base font-medium text-center text-black">
          {props.lastMessage}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default UserChat;
