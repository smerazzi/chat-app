import * as RNLocalize from 'react-native-localize';
import { I18n } from 'i18n-js';
import it from '../lang/it.json';
import en from '../lang/it.json';

// Set the key-value pairs for the different languages you want to support.
const i18n = new I18n({
  en: en,
  it: it,
});

// Set the locale once at the beginning of your app.
i18n.locale = RNLocalize.getLocales()[0].languageCode;
i18n.enableFallback = true;

export default i18n;
