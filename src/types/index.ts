import { NativeStackScreenProps } from '@react-navigation/native-stack';

type RootStackParamList = {
  Home: undefined;
  Chat: { name: string };
};

type HomeScreenStackProps = NativeStackScreenProps<
  RootStackParamList,
  'Home',
  'home'
>;

type ChatScreenStackProps = NativeStackScreenProps<
  RootStackParamList,
  'Chat',
  'chat'
>;

export type { RootStackParamList, HomeScreenStackProps, ChatScreenStackProps };
